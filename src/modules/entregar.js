import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		datosPersona:'',
		firma:'',
		// pendientes:'',
		// datosUsuario:'',
	},

	mutations:{
		SAVEDATOS(state, datos){
			state.datosPersona = datos
		},

		FIRMA(state, formdata){
			state.firma = formdata
		}

	},
	actions:{

		//Se guardan los datos de usuario y de la entrega
		saveDatos({commit, dispatch}, datos){
			commit('SAVEDATOS',datos)
		},

		//Guardo la imagen de la firma
		saveFirma({commit, dispatch}, formada){
			commit('FIRMA',formada)
		},

		addEntrega_idb({commit, dispatch}, data){
			const dbName = "solemexV1";
      //abres la base de datos (nombre, version)
      var request = indexedDB.open(dbName, 2);
      request.onerror = function(event) {
        alert("Why didn't you allow my web app to use IndexedDB?!");
      };
      request.onsuccess = function(event) {
        //Configurar lo que vas a realizar con esa tabla ( tabla, reescribir-add).la tabla
        var customerObjectStore = event.target.result.transaction("entregar", "readwrite").objectStore("entregar");
        //Agregar lso datos
        customerObjectStore.add(data)
      };
		},

    //internet
		addEntrega_internet({commit, dispatch}, data){
			//retornar una promesa (resolve, rejecet)
			return new Promise((resolve, reject) => {
			  Vue.http.post('api/v1/entregar',{
					id:             data.id,
	      	latitude:       data.latitude,
	      	longitude:      data.longitude,
	      	nomrecibe:      data.nomrecibe,
	      	parentesco:     data.parentesco,
	      	tipoid:         data.tipoid,
	      	numid:          data.numid,
	      	fecha_entrega:  data.fecha_entrega,
	      	estatus:        data.estatus,
	      	nomestatus:     data.nomestatus,
	      	hora_entrega:   data.hora_entrega,
	      	tipo_entrega:   data.tipo_entrega,
	      	idsucursal:     data.idsucursal,
	      	idrem:       data.idremesa
				})
				.then(respuesta=>{return respuesta.json()})
				.then(respuestaJson=>{
					if(respuestaJson == null){
					  
		      }else{
		      	// router.push({name: 'menu'}) 
		      }
		    }, error => {
        	reject(error)
      	})
      })
			
		},

		addCorreos({commit, dispatch}, correo){
			const dbName = "solemexV1";
      //abres la base de datos (nombre, version)
      var request = indexedDB.open(dbName, 2);
      request.onerror = function(event) {
        alert("Why didn't you allow my web app to use IndexedDB?!");
      };
      request.onsuccess = function(event) {
        //Configurar lo que vas a realizar con esa tabla ( tabla, reescribir-add).la tabla
        var customerObjectStore = event.target.result.transaction("correos", "readwrite").objectStore("correos");
        //Agregar lso datos
        customerObjectStore.add(correo)
      };
		},

},

	getters:{
		getDatosEntrega(state){
		  return state.datosPersona
		},

		getFirma(state){
		  return state.firma
		},


	}
}