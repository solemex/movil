import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import VueIdb from 'vue-idb'
import VueResource from 'vue-resource'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

//firma
import VueSignaturePad from 'vue-signature-pad';
 
 
Vue.config.productionTip = false
Vue.use(VueResource, VueSignaturePad)

//esta es la configración para una url constante la cual se ejecuta antes del vue
Vue.http.options.root = 'https://solemex.ws/sol'
 // Vue.http.options.root = 'http://localhost:8085/sol'

//Vue.http.options.root = 'http://localhost/soporte-sait/'

Vue.http.interceptors.push((request, next) => {
 //request.headers.set('Authorization', 'Bearer '+JSON.parse(localStorage.apiKey_admin))
  request.headers.set('Accept', 'application/json')
  next()
});



import 'vue-googlemaps/dist/vue-googlemaps.css'
import VueGoogleMaps from 'vue-googlemaps'

Vue.use(VueIdb,VueGoogleMaps, {
    load: {
      // Google API key
      apiKey: 'AIzaSyBqWhazMohP23r6IpHa5ybtqruNmw5tulE',
      // Enable more Google Maps libraries here
      libraries: ['places'],
      // Use new renderer
      useBetaRenderer: false,
    }
})


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
