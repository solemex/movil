import Vue       from 'vue'
import Vuex      from 'vuex'
import avisosidb from '@/modules/avisosidb'

import login       from '@/modules/login'
import prioridades from '@/modules/prioridades'
import pendientes  from '@/modules/pendientes'
import intentos    from '@/modules/intentos'  
import entregar    from '@/modules/entregar'  
import avisos      from '@/modules/avisos'
import internet    from '@/modules/internet'
import imagenes    from '@/modules/imagenes'
import actualizar  from '@/modules/actualizar'
    

/* Librerias para utilizar el google maps */
import 'vue-googlemaps/dist/vue-googlemaps.css'
import VueGoogleMaps from 'vue-googlemaps'
 

 // import db from '@/modules/indexedDBService.js'
// Vue.use(VueGoogleMaps, {
//   load: {
//     // Google API key
//     apiKey: 'your-google-api-key',
//     // Enable more Google Maps libraries here
//     libraries: ['places'],
//     // Use new renderer
//     useBetaRenderer: false,
//   },
// })

Vue.use(Vuex) 


export default new Vuex.Store({
	modules:{
		avisosidb,
		login,
		prioridades,
		pendientes,
		intentos,
		entregar,
		avisos,
		internet,
		imagenes,
		actualizar,
	}
		
})
