import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		internet: false,
	},

	mutations:{

	},
	actions:{
    //internet
		internet({commit, dispatch}, data){
			//retornar una promesa (resolve, rejecet)
			return new Promise((resolve, reject) => {
				//Se hace un post a una api solo para ver si hay internet
			  Vue.http.get('api/v1/clientes').then(function(response){
			  	//verificamos que el estatus de la respuesta haya sido un 200 (hay internet)
			  	if(response.status === 200){
			  		//resolve retorna un true
			  		resolve(true)
			  	}
			  }).catch(function(error){
			  	//verificamos el error, si existe un error retornamos directamente el fasle con el resolve
			  		resolve(false)
        })
		  })
		}
  },

	getters:{

	}
}