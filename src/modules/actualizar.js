import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
	
	},

	mutations:{


	},
	actions:{
    //función para limpiar la base de datos
    deleteIDB({dispatch}){
      //Verifico primero que el indexdb este disponible en la aplicación
      if (!window.indexedDB) {
          console.log('Tu version de dispositivo no soporta conexiones sin internet');
          return;
        }else{
          //creo mi variable db que tendra la conexión a la base de dato
          var db;
          //request será el resultado de intenetar abrir la base de datos del indexdb
          var request = window.indexedDB.open('solemexV1',2);
          //Si la conexión fu exitosa se ejecuta onsuccess
          request.onsuccess= function(e){
            //db obtendra el resultado de esa conexión
            db = request.result;
            // se crea una variable pendientes 
            // que obtendrá el resultado de limpiar la tabla de pendientes
            var pendientes = db.transaction(["pendientes"], "readwrite").objectStore("pendientes").clear();
            //Si se ejecuto con exito de ejecuta la siguiente función
            pendientes.onsuccess = function(event) {
              //ahora que sabes que si se ejecuto continuamos creando otra variable "entregar" 
              //la cual obtendrá el resultado de limpiar la tabla de entregar
              var entregar = db.transaction(["entregar"], "readwrite").objectStore("entregar").clear();
              entregar.onsuccess = function(event) {
               var avisos = db.transaction(["avisos"], "readwrite").objectStore("avisos").clear();
                avisos.onsuccess = function(event) {
                 // var avisos = db.transaction(["pendientes"], "readwrite").objectStore("pendientes").clear();
                };
                //Si hubo un error simplemente nos avisa
                avisos.onerror = function(event) {
                  console.log('Problemas para limpiar tabla');
                };
              };
              //Si hubo un error simplemente nos avisa
              entregar.onerror = function(event) {
                console.log('Problemas para limpiar tabla');
              };
            };
            //Si hubo un error simplemente nos avisa
            pendientes.onerror = function(event) {
              console.log('Problemas para limpiar tabla');
            };
          }
          //Si hubo un error simplemente nos avisa
          request.onerror = function(e){
            console.log('error')
          }
          //Sirve para actualizar la base de datos
          request.onupgradeneeded = function(e){

          }
        }
    },

    createIDB(){
      return new Promise((resolve, reject) => {
        //Verifico primero que el indexdb este disponible en la aplicación
        if (!window.indexedDB) {
          console.log('Tu version de dispositivo no soporta conexiones sin internet');
          return;
        }else{
          //creo mi variable db que tendra la conexión a la base de dato
          var db;
          //request será el resultado de intenetar abrir la base de datos del indexdb
          var request = window.indexedDB.open('solemexV1',2);
           //Si se ejecuto con exito de ejecuta la siguiente función
          request.onsuccess= function(e){
            db = request.result;
          }
           //Si no se ejecuto con exito de ejecuta la siguiente función
          request.onerror = function(e){
            console.log(e);
          }
           //Esta función es para actualizar el idb
          request.onupgradeneeded = function(e){
            //db contendrá la funciones necesarias para actualizar la base de datos o en este caso crearla
            db = e.target.result;

            // Version 1
            //si db.la tabla con x nombre contiene algo
            if(!db.objectStoreNames.contains("avisos")) {
              //se crea una variable con las opciones, keyPeth es para crear la llave, y autoincrement es falsa ya que nosotros 
              //le asignaremos su propia llave, para más informacion investigar sobre idb llaves
              var opciones = {keyPath:'Id',autoIncrement:false};
              // ahora se crea la tabla (nombre, opciones)
              var objectStorage = db.createObjectStore("avisos",opciones);
            }
            //si db.la tabla con x nombre contiene algo 
            if(!db.objectStoreNames.contains("pendientes")) {
              //se crea una variable con las opciones, keyPeth es para crear la llave, y autoincrement es falsa ya que nosotros 
              //le asignaremos su propia llave, para más informacion investigar sobre idb llaves
              var opciones = {keyPath:'Numguia',autoIncrement:false};
              // ahora se crea la tabla (nombre, opciones)
              var objectStorage = db.createObjectStore("pendientes",opciones);
            }
            //si db.la tabla con x nombre contiene algo
            if(!db.objectStoreNames.contains("intentos")) {
              //se crea una variable con las opciones, keyPeth es para crear la llave, y autoincrement es falsa ya que nosotros 
              //le asignaremos su propia llave, para más informacion investigar sobre idb llaves
              var opciones = {keyPath:'Numguia',autoIncrement:false};
              // ahora se crea la tabla (nombre, opciones)
              var objectStorage = db.createObjectStore("intentos",opciones);
            }
            //si db.la tabla con x nombre contiene algo
            if(!db.objectStoreNames.contains("entregar")) {
              //se crea una variable con las opciones, keyPeth es para crear la llave, y autoincrement es falsa ya que nosotros 
              //le asignaremos su propia llave, para más informacion investigar sobre idb llaves
              var opciones = {keyPath:'Numguia',autoIncrement:false};
              // ahora se crea la tabla (nombre, opciones)
              var objectStorage = db.createObjectStore("entregar",opciones);
            }
            //si db.la tabla con x nombre contiene algo
            if(!db.objectStoreNames.contains("prioridad")) {
              //se crea una variable con las opciones, keyPeth es para crear la llave, y autoincrement es falsa ya que nosotros 
              //le asignaremos su propia llave, para más informacion investigar sobre idb llaves
              var opciones = {keyPath:'Numguia',autoIncrement:false};
              // ahora se crea la tabla (nombre, opciones)
              var objectStorage = db.createObjectStore("prioridad",opciones);
            }
            //si db.la tabla con x nombre contiene algo
            if(!db.objectStoreNames.contains("imagenes")) {
              //le asignaremos su propia llave, para más informacion investigar sobre idb llaves
              var opciones = {autoIncrement:false};
              // ahora se crea la tabla (nombre, opciones)
              var objectStorage = db.createObjectStore("imagenes",opciones);
            }
            //si db.la tabla con x nombre contiene algo
            if(!db.objectStoreNames.contains("correos")) {
              //le asignaremos su propia llave, para más informacion investigar sobre idb llaves
              var opciones = {autoIncrement:false};
              // ahora se crea la tabla (nombre, opciones)
              var objectStorage = db.createObjectStore("correos",opciones);
            }
            //si db.la tabla con x nombre contiene algo
            if(!db.objectStoreNames.contains("sucursales")) {
              //le asignaremos su propia llave, para más informacion investigar sobre idb llaves
              var opciones = {autoIncrement:false};
              // ahora se crea la tabla (nombre, opciones)
              var objectStorage = db.createObjectStore("sucursales",opciones);
            }

          }
        }
        resolve(true)
      })
    },

    //Función para actualizar la base de datos
		actualizaridb({dispatch}){
			// dispatch('guardarIntentos')
			dispatch('guardarImagenes')
			dispatch('guardarEntregas')
		},

    //Función para guardar los intentos, esta recibe un parametro 
		guardarIntentos({commit, dispatch}, data){
			return new Promise((resolve, reject) => {
        //se crea una variable con el nombre de la base de datos
        const dbName = "solemexV1";
        //abres la base de datos (nombre, version)
        var request = indexedDB.open(dbName, 2);
        //Si no se puede abrir se ejecuta onerrror
        request.onerror = function(event) {
          alert("Why didn't you allow my web app to use IndexedDB?!");
        };
        //Si se abrio la conexión se ejecuta onsuccess
        request.onsuccess = function(event) {
          //obhectStore contendrá en resultad de abrir la tabla de intentos
          var objectStore = event.target.result.transaction("intentos").objectStore("intentos");
          //Se crea una variable de tipo arreglo para guardar todo lo que contiene la tabla
          var intentos = []
          //si se pudo abrir... se ejecuta un cursos para onsuccess
          objectStore.openCursor().onsuccess = function(event) {
            // se crea un cursos con todo lo que tiene la tabla
            var cursor = event.target.result;
            //este es un ciclo que se ejecuta mientras cursor sea versadero
            if (cursor) {
              //se le agrega a intentos todo lo que contiene la tabla
              intentos.push(cursor.value)
              cursor.continue();
            }
            else {
              //al terminar el ciclo el resolve retornara todo el arreglo de intentos 
              resolve(intentos)
            }
          };
        };
      })
		},

		guardarImagenes({commit, dispatch}, data){
			return new Promise((resolve, reject) => {
        const dbName = "solemexV1";
        //abres la base de datos (nombre, version)
        var request = indexedDB.open(dbName, 2);
        //Si no se puede abrir se ejecuta onerrror
        request.onerror = function(event) {
          alert("Why didn't you allow my web app to use IndexedDB?!");
        };
        //Si se abrio la conexión se ejecuta onsuccess
        request.onsuccess = function(event) {
          //obhectStore contendrá en resultad de abrir la tabla de imagenes
          var objectStore = event.target.result.transaction("imagenes").objectStore("imagenes");
          //Se crea una variable de tipo arreglo para guardar todo lo que contiene la tabla
          var imagenes = []
          //si se pudo abrir... se ejecuta un cursos para onsuccess
          objectStore.openCursor().onsuccess = function(event) {
            // se crea un cursos con todo lo que tiene la tabla
            var cursor = event.target.result;
            //este es un ciclo que se ejecuta mientras cursor sea versadero
            if (cursor) {
              //se le agrega a imagenes todo lo que contiene la tabla
              imagenes.push(cursor.value)
              cursor.continue();
            }
            else {
              //al terminar el ciclo el resolve retornara todo el arreglo de imagenes 
              resolve(imagenes)
            }
          };
        };
      })
		},

		guardarEntregas({commit, dispatch}, data){
			return new Promise((resolve, reject) => {
        const dbName = "solemexV1";
        //abres la base de datos (nombre, version)
        var request = indexedDB.open(dbName, 2);
        //Si no se puede abrir se ejecuta onerrror
        request.onerror = function(event) {
          alert("Why didn't you allow my web app to use IndexedDB?!");
        };
        //Si se abrio la conexión se ejecuta onsuccess
        request.onsuccess = function(event) {
          //obhectStore contendrá en resultad de abrir la tabla de entregas
          var objectStore = event.target.result.transaction("entregar").objectStore("entregar");
          //Se crea una variable de tipo arreglo para guardar todo lo que contiene la tabla
          var entregas = []
          //si se pudo abrir... se ejecuta un cursos para onsuccess
          objectStore.openCursor().onsuccess = function(event) {
            // se crea un cursos con todo lo que tiene la tabla
            var cursor = event.target.result;
            //este es un ciclo que se ejecuta mientras cursor sea versadero
            if (cursor) {
              //se le agrega a entregas todo lo que contiene la tabla
              entregas.push(cursor.value)
              cursor.continue();
            }
            else {
              //al terminar el ciclo el resolve retornara todo el arreglo de entregas 
              resolve(entregas)
            }
          };
        };
      })
		},

    guardarCorreos({commit, dispatch}, data){
      return new Promise((resolve, reject) => {
        const dbName = "solemexV1";
        //abres la base de datos (nombre, version)
        var request = indexedDB.open(dbName, 2);
        //Si no se puede abrir se ejecuta onerrror
        request.onerror = function(event) {
          alert("Why didn't you allow my web app to use IndexedDB?!");
        };
        //Si se abrio la conexión se ejecuta onsuccess
        request.onsuccess = function(event) {
          //obhectStore contendrá en resultad de abrir la tabla de correos
          var objectStore = event.target.result.transaction("correos").objectStore("correos");
          //Se crea una variable de tipo arreglo para guardar todo lo que contiene la tabla
          var correos = []
          //si se pudo abrir... se ejecuta un cursos para onsuccess
          objectStore.openCursor().onsuccess = function(event) {
            // se crea un cursos con todo lo que tiene la tabla
            var cursor = event.target.result;
            //este es un ciclo que se ejecuta mientras cursor sea versadero
            if (cursor) {
              //se le agrega a correos todo lo que contiene la tabla
              correos.push(cursor.value)
              cursor.continue();
            }
            else {
              //al terminar el ciclo el resolve retornara todo el arreglo de correos 
              resolve(correos)
            }
          };
        };
      })
    },

    //agregar intento sin internet
		addIntento({commit, dispatch}, formdata){
			//retornar una promesa (resolve, rejecet)
			return new Promise((resolve, reject) => {
			  Vue.http.post('api/v1/addintento', formdata )
				.then(respuesta=>{return respuesta.json()})
				.then(respuestaJson=>{
					if(respuestaJson == null){
					  
		      }else{
		      	router.push({name: 'menu'}) 
		      }
		    }, error => {
        	reject(error)
      	})
      })
		},


},

	getters:{

	}
}