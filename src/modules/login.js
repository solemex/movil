import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		login:false,
		datosUsuario:'',
	},

	mutations:{
		LOGEADO(state, value){
			state.login = value
		},
		DATOS_USUARIO(state, datosUsuario){
      state.datosUsuario = datosUsuario
		},

	},
	actions:{
		salirUser({commit, dispatch}, value){
			commit('LOGEADO', value)
			commit('DATOS_USUARIO','')
		},

		//ejecuta la api y llama a la mutcion traeInfo para poder mandarle la información obtenida de la api
		validarUser({commit, dispatch}, usuario){
      var md5 = require('md5');

			//retornar una promesa (resolve, rejecet)
			return new Promise((resolve, reject) => {
				//No se utiliza el this por que no esta dentro de un componente, tampoco se pone el signo de pesos por que no es una variable global
				//Se crea un formdata que sera el parametro que se le mandara a la api
				const formData = {
		  		'nomuser': usuario.nomuser,
					'password': md5(usuario.password)
				}
				//Se hace el post para verificar los datos de el login y se le manda como parametro el formdata
				Vue.http.post('api/v1/loginapp',formData)
				//Se convierte la respuesta a json 
				.then(respuesta=>{return respuesta.json()})
				//y ahora traemos el json de esa respuesta
				.then(respuestaJson=>{
					//si la respuesta es null
					if(respuestaJson == null){
						//retornamos en la promesa la respues
						resolve(respuestaJson) 
						return true
					}else{
						//si no es null, se guardan los datos que retorno la api en DATOS_USUARIO
		       	commit('DATOS_USUARIO',respuestaJson[0])
		       	//Y tambien se guarda el estado de que esta logeado
		       	commit('LOGEADO', true)
		       	resolve(respuestaJson)
	       	}
	     	}, error => {
	     		//se retorna un error
	       	reject(error)
	     	})
			})
		},
},

	getters:{
		getLogeado(state){
		  return state.login
		},

		getdatosUsuario(state){
			return state.datosUsuario
		},

	}
}