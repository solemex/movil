import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
  namespaced: true,
  state:{
    avisos: '',
    aviso:'',
  },

  mutations:{
    //Mutación para guardar los avisos
    AVISOS(state, value){
      state.avisos = value
    },
    //Mutación para mostrar el aviso de una tarjeta
    AVISO(state, value){
      state.aviso = value
    },

  },
  
  actions:{
    //Función para guardar un aviso y mostrarlo
    vistaAviso({commit, dispatch}, payload){
      commit('AVISO',payload)
      router.push({name:'aviso'})
    },

    //Función para traer avisos 
    traerAvisos({commit, dispatch}, idmensajero){
      //retornar una promesa (resolve, rejecet)
      return new Promise((resolve, reject) => {
        //hacemos el post y mandamos como parametro el idmensajero
        Vue.http.post('api/v1/avisosxmens',{'idmensajero': idmensajero})
        //convertimos la respuesta a json
        .then(respuesta=>{return respuesta.json()})
        //retornamos el json
        .then(respuestaJson=>{
          //mandamos a llamar la funcion para cargar avisos al idb y le mandamos la respuesta como parametro
          dispatch('addAvisosIdb', respuestaJson)
          //resolve = false pero no es necesario
          resolve(false) 
        }, error => {
          reject(error)
        })
      })
    },

    //Función para agregar los avisos al indexdb
    addAvisosIdb({commit, dispatch},value){
      const dbName = "solemexV1";
      //abres la base de datos (nombre, version)
      var request = indexedDB.open(dbName, 2);
      //función que se ejecuta cuando no se pudo abrir la base de datos
      request.onerror = function(event) {
        alert("Why didn't you allow my web app to use IndexedDB?!");
      };
      //función que se ejecuta cuando si se pudo abrir la base de datos
      request.onsuccess = function(event) {
        //Configurar lo que vas a realizar con esa tabla ( tabla, reescribir-add).la tabla
        var customerObjectStore = event.target.result.transaction("avisos", "readwrite").objectStore("avisos");
        //Agregar lso datos
        for (var i = value.length - 1; i >= 0; i--) {
          // customerObjectStore.add(value[i]);
          customerObjectStore.add({
            Aviso:        value[i].Aviso, 
            Estatus:      value[i].Estatus,
            Fecha:        value[i].Fecha,
            Hora:         value[i].Hora,
            Hora_visto:   value[i].Hora_visto,
            Id:           value[i].Id,
            Idguia:       value[i].Idguia,
            Idmensajero:  value[i].Idmensajero,
            Idusuario:    value[i].Idusuario,
            Numguia:      value[i].Numguia,
            fecha_visto:  value[i].fecha_visto
          });
        }
      };
      dispatch('traerAvisosIdb')
    },

    //Función para traer los avisos de indexdb
    traerAvisosIdb({commit, dispatch}){
      return new Promise((resolve, reject) => {
        const dbName = "solemexV1";
        //abres la base de datos (nombre, version)
        var request = indexedDB.open(dbName, 2);
        //función que se ejecuta cuando no se pudo abrir la base de datos 
        request.onerror = function(event) {
          alert("Why didn't you allow my web app to use IndexedDB?!");
        };
        //fución que se ejecuta cuando si se pudo abrir la base de datos
        request.onsuccess = function(event) {
          //transaction (Las  tablas que vas a utilizar)
          var objectStore = event.target.result.transaction("avisos").objectStore("avisos");
          //se crea una variable que contendra los datos que estan en la tabla
          var datos = []

          //se crea un onsuccess para un cursos
          objectStore.openCursor().onsuccess = function(event) {
            //se crea un cursor con el resultado
            var cursor = event.target.result;
            //ciclo para el cursos
            if (cursor) {
              //se agregar el valor del cursor en datos
              datos.push(cursor.value)
              cursor.continue();
            }
            else {
              //se le manda a la mutación los datos para se guardados
              console.log('datos',datos)
              commit('AVISOS',datos)
              resolve(true)
            }
          };
        };
      })
    }
  },

  getters:{
    //mini función para obetener el state de avisos
    getAvisos(state){
      return state.avisos
    },
    //Traer la información de un aviso
    getAviso(state){
      return state.aviso
    }
  }
}
