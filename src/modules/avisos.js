import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		avisos:'',
		datosUsuario:'',
	},

	mutations:{
		AVISOS(state, value){
			state.avisos = value
		},
		DATOS_USUARIO(state, datosUsuario){
      state.datosUsuario = datosUsuario
		},

	},
	actions:{
		salirUser({commit, dispatch}, value){
			commit('AVISOS', value)
			commit('DATOS_USUARIO','')
		},
		//Función para cargar los avisos de
		traerAvisos({commit, dispatch}, usuario){
			//retornar una promesa (resolve, rejecet)
			return new Promise((resolve, reject) => {
			  Vue.http.post('api/v1/avisosxmens',{'idmensajero': usuario})
				.then(respuesta=>{return respuesta.json()})
				.then(respuestaJson=>{
		      	commit('AVISOS',respuestaJson)
		      	resolve(false) 
		    }, error => {
        	reject(error)
      	})
      })
		},


},

	getters:{
		getAvisos(state){
		  return state.avisos
		},

		getdatosUsuario(state){
			return state.datosUsuario
		},

	}
}