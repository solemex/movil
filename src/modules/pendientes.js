import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		pendientes:[],
    sucursales:[]
	},

	mutations:{
		PENDIENTES(state, value){
			state.pendientes = value
		},

    SUCURSALES(state, value){
      state.sucursales = value
    },

	},
	actions:{
    //Función para traer pendientes con internet
		traerPendientes({commit, dispatch}, idmensajero){
			//retornar una promesa (resolve, rejecet)
			return new Promise((resolve, reject) => {
        //se hace el post y se manda como parametro el idmensajero
			  Vue.http.post('api/v1/pendxmensajero',{
					'idmensajero': idmensajero
				})
        //se convierte la respuesta a json
				.then(respuesta=>{return respuesta.json()})
        //retornamos el respuesta ya convertida
				.then(respuestaJson=>{
          console.log('pendientes',respuestaJson)
				  dispatch('addPendientesidb', respuestaJson)
				// router.push({name: 'pendientes'}) 
			}, error => {
			    reject(error)
		    })
	    })
		},

    //función para agregar los dato traidos de la api al indexdb
		addPendientesidb({commit, dispatch},value){
      const dbName = "solemexV1";
      //abres la base de datos (nombre, version)
      var request = indexedDB.open(dbName, 2);
      //función que se ejecuta si no se pudo abrir la base de datos
      request.onerror = function(event) {
        alert("Why didn't you allow my web app to use IndexedDB?!");
      };
      //función que se ejecuta si se pudo abrir la pelicula
      request.onsuccess = function(event) {
        //Configurar lo que vas a realizar con esa tabla ( tabla, reescribir-add).la tabla
        var customerObjectStore = event.target.result.transaction("pendientes", "readwrite").objectStore("pendientes");
        //Agregar lso datos
        for (var i = value.length - 1; i >= 0; i--) {
          customerObjectStore.add({
            Id:            value[i].Id, 
            idremesa:      value[i].Idremesa,
            Estatus:       value[i].Estatus,
            Idcliente:     value[i].Idcliente,
            Numguia:       value[i].Numguia,
            Numrem:        value[i].Numrem,
            mensajero:     value[i].Mensajero,
            Prioridad:     value[i].Prioridad,
            email:         value[i].Email,
            nomcli:        value[i].Nomcli,
            num_intentos:  value[i].Num_intentos,
            numsuc:        value[i].Numsuc,
            nomsuc:        value[i].Nomsuc,
            direccion:     value[i].direccion,
            colonia:       value[i].Colonia,
            ciudad:        value[i].Ciudad,
            cp:            value[i].Cp,
            estado:        value[i].Estado,
            telefono:      value[i].Telefono,
            tipoent:       value[i].tipoent
          });
        }
      };
      dispatch('traerPendientesIdb')
    },  

    //Función para traer los datos de la tabla del indexdb
    traerPendientesIdb({commit, dispatch}){
      return new Promise((resolve, reject) => {
        const dbName = "solemexV1";
        //abres la base de datos (nombre, version)
        var request = indexedDB.open(dbName, 2);
        //función que se ejecuta cuando si no se puede abrir la base de datos
        request.onerror = function(event) {
          alert("Why didn't you allow my web app to use IndexedDB?!");
        };
        //función que se ejecuta cuando si se puede abrir la base de datos
        request.onsuccess = function(event) {
          //transaction (Las  tablas que vas a utilizar)
          var objectStore = event.target.result.transaction("pendientes").objectStore("pendientes");
          //se crea una variable de tipo arreglo para guardar los datos
          var datos = []
          //se crea un onsuccess de un cursos
          objectStore.openCursor().onsuccess = function(event) {
            //se crea un cursor con el reusltado de la función
            var cursor = event.target.result;
            //ciclo que se ejecuta cuando el cursos aun no termina el recorrido
            if (cursor) {
              // se agrega el valor de el cursos al arreglo
              datos.push({
                key:          cursor.key,
                Estatus:      cursor.value.Estatus,
                Id:           cursor.value.Id,
                Idcliente:    cursor.value.Idcliente,
                Numguia:      cursor.value.Numguia,
                Numrem:       cursor.value.Numrem,
                Prioridad:    cursor.value.Prioridad,
                email:        cursor.value.email,
                idremesa:     cursor.value.idremesa,
                mensajero:    cursor.value.mensajero,
                nomcli:       cursor.value.nomcli,
                num_intentos: cursor.value.num_intentos,
                numsuc:       cursor.value.numsuc,
                nomsuc:       cursor.value.nomsuc,
                direccion:    cursor.value.direccion,
                colonia:      cursor.value.colonia,
                ciudad:       cursor.value.ciudad,
                estado:       cursor.value.estado,
                cp:           cursor.value.cp,
                telefono:     cursor.value.telefono,
                tipoent:      cursor.value.tipoent
              }) 
              cursor.continue();
            }
            else {
              console.log('data pendientes', datos)
              //Se agregan los datos en pendientes 
              commit('PENDIENTES',datos)
              //se retorna true para indicar que ya termino
              resolve(true)
            }
          };
        };
      })
    },

    deletePendienteidb({commit, dispatch}, folio){
      const dbName = "solemexV1";
      //abres la base de datos (nombre, version)
      var request = indexedDB.open(dbName, 2);
      
      request.onerror = function(event) {
        alert("Why didn't you allow my web app to use IndexedDB?!");
      };

      request.onsuccess = function(event) {
        //Configurar lo que vas a realizar con esa tabla ( tabla, reescribir-add).la tabla
        var customerObjectStore = event.target.result.transaction("pendientes", "readwrite").objectStore("Numguia").delete(folio);
        customerObjectStore.onsuccess = function(event) {
        };
        dispatch('traerPendientesIdb')
      };
    },

},

	getters:{
		getPendientes(state){
		  return state.pendientes
		},

	}
}
