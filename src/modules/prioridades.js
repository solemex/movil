import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		prioridad:'',
		datosUsuario:'',
	},

	mutations:{
		PRIORIDAD(state, value){
			state.prioridad = value
		},
		DATOS_USUARIO(state, datosUsuario){
      state.datosUsuario = datosUsuario
		},

	},
	actions:{
		salirUser({commit, dispatch}, value){
			commit('LOGEADO', value)
			commit('DATOS_USUARIO','')
		},
		traerPrioridades({commit, dispatch}, idmensajero){
			//retornar una promesa (resolve, rejecet)
			return new Promise((resolve, reject) => {
			  Vue.http.post('api/v1/prioridadxmens',{
					'idmensajero': idmensajero
				})
				.then(respuesta=>{return respuesta.json()})
				.then(respuestaJson=>{
		      	dispatch('addPrioridadesidb',respuestaJson)
		    }, error => {
        	reject(error)
      	})
      })
		},


		//ejecuta la api y llama a la mutcion traeInfo para poder mandarle la información obtenida de la api
		addPrioridadesidb({commit, dispatch}, value){
			const dbName = "solemexV1";
      //abres la base de datos (nombre, version)
      var request = indexedDB.open(dbName, 2);
      request.onerror = function(event) {
        alert("Why didn't you allow my web app to use IndexedDB?!");
      };
      request.onsuccess = function(event) {
        //Configurar lo que vas a realizar con esa tabla ( tabla, reescribir-add).la tabla
        var customerObjectStore = event.target.result.transaction("prioridad", "readwrite").objectStore("prioridad");
        //Agregar lso datos
        for (var i = value.length - 1; i >= 0; i--) {
          customerObjectStore.add({
            Estatus:      value[i].Estatus,
            Id:           value[i].Id, 
            Idcliente:    value[i].Idcliente,
            Numguia:      value[i].Numguia,
            Numrem:       value[i].Numrem,
            Prioridad:    value[i].Prioridad,
            idremesa:     value[i].idremesa,
            mensajero:    value[i].mensajero
          });
        }
        dispatch('traerPrioridadesidb')
      };
		},

		traerPrioridadesidb({dispatch, commit}, datos){
			return new Promise((resolve, reject) => {
        const dbName = "solemexV1";
        //abres la base de datos (nombre, version)
        var request = indexedDB.open(dbName, 2);
        request.onerror = function(event) {
          alert("Why didn't you allow my web app to use IndexedDB?!");
        };
        request.onsuccess = function(event) {
          //transaction (Las  tablas que vas a utilizar)
          var objectStore = event.target.result.transaction("prioridad").objectStore("prioridad");
          var datos
          datos = []

          objectStore.openCursor().onsuccess = function(event) {
            var cursor = event.target.result;
            if (cursor) {
              datos.push(cursor.value)
              cursor.continue();
            }
            else {
              commit('PRIORIDAD',datos)
              resolve(true)
            }
          };
        };
      })
		},

},

	getters:{
		getPrioridades(state){
		  return state.prioridad
		},

		getdatosUsuario(state){
			return state.datosUsuario
		},

	}
}