import Vue from 'vue'
import Router from 'vue-router'

//Vista para las pruebas
import Prueba         from '@/components/Prueba.vue'
import IndexDB        from '@/components/IndexDB.vue'
import Ubicacion      from '@/components/Ubicacion.vue'
import Camara         from '@/components/Camara.vue'
import SubirImagen    from '@/components/SubirImagen.vue'
import PruebaCorreos  from '@/components/PruebaCorreos.vue'

import Avisos        from '@/views/avisos/Avisos.vue'
import Aviso         from '@/views/avisos/Aviso.vue'
import Devolucion    from '@/views/devolucion/Devolucion.vue'

//Entregar
import Entregar      from '@/views/entregar/Entregar.vue'
import Fotos         from '@/views/entregar/Fotos.vue'
import Firma         from '@/views/entregar/Firma.vue'

import Pendientes    from '@/views/pendientes/Pendientes.vue'
import Prioridad     from '@/views/prioridad/Prioridad.vue'
import Login         from '@/views/usuarios/Login.vue'
import Registro      from '@/views/usuarios/Registro.vue'
import MiRuta        from '@/views/ruta/MiRuta.vue'
import Menu          from '@/views/Menu.vue'
import Localizacion  from '@/views/Localizacion.vue'
import Informacion   from '@/views/Informacion.vue'
import App           from '@/App.vue'


Vue.use(Router)

export default new Router({
  mode: process.env.CORDOVA_PLATFORM ? 'hash' :  'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/indexdb',      name: 'indexdb',      component: IndexDB    },
    { path: '/prueba',       name: 'prueba',       component: Prueba     },
    { path: '/ubicacion',    name: 'ubicacion',    component: Ubicacion  },
    { path: '/camara',       name: 'camara',       component: Camara     },
    { path: '/avisos',       name: 'avisos',       component: Avisos     },
    { path: '/aviso',        name: 'aviso',        component: Aviso      },
    { path: '/devolucion',   name: 'devolucion',   component: Devolucion },

    { path: '/entregar',     name: 'entregar',     component: Entregar   },
    { path: '/firma',        name: 'firma',        component: Firma      },
    { path: '/fotos',        name: 'fotos',        component: Fotos      },

    { path: '/informacion',  name: 'informacion',  component: Informacion      },

    { path: '/pendientes',   name: 'pendientes',   component: Pendientes },
    { path: '/prioridad',    name: 'prioridad',    component: Prioridad  },
    { path: '/login',        name: 'login',        component: Login      },
    { path: '/registro',     name: 'registro',     component: Registro   },
    { path: '/menu',         name: 'menu',         component: Menu       },
    { path: '/localizacion', name: 'localizacion', component: Localizacion    },
    { path: '/miruta',       name: 'miruta',       component: MiRuta     },
    { path: '/subirimagen',  name: 'subirimagen',  component: SubirImagen      },
    { path: '/pruebaCorreos',  name: 'pruebaCorreos',  component: PruebaCorreos      },

 
  ]
})
